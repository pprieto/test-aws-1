#!/usr/bin/env nextflow

params.base_dir="/users/cn/mhatzou/Datasets/HomFam/seqs_noBZXU_noDuplicates/"
params.out_dir="MEGA_NF"
params.names="hip scorptoxin".tokenize()

Channel.from(params.names)
       .map{ tuple(it, file("${params.base_dir}/${it}_uniq.fa")) }
       .set{ file_names }

params.value=5000


process align{
  publishDir params.out_dir

  input:
      set val(name), file(seq_file) from file_names 
  output:
      file "${name}.fa" into foo
  
  
  """ 
     tea -i $seq_file -o ${name}.fa -d --cluster_size 2 --cluster_number 5000 > /dev/null
  """

}
